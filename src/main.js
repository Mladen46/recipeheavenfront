/**
 * main.js
 *
 * Bootstraps Vuetify and other plugins then mounts the App`
 */

// Plugins
import { registerPlugins } from '@/plugins'
import axios from 'axios';

// Components
import App from './App.vue'

// Composables
import { createApp } from 'vue'

// Configuration
axios.defaults.baseURL = import.meta.env.VITE_API_URL;

const app = createApp(App)

registerPlugins(app)

app.mount('#app')
