
/**
 * router/index.ts
 *
 * Automatic routes for `./src/pages/*.vue`
 */

// Composables
import { createRouter, createWebHistory } from 'vue-router'
import HomePage from '@/pages/HomePage.vue'
import AddRecipePage from '@/pages/AddRecipePage.vue'
import RecipeDetailsPage from '@/pages/RecipeDetailsPage.vue'

const routes = [
  {
    path: "/",
    name: "HomePage",
    component: HomePage
  },
  {
    path: "/recipe/:id",
    name: "RecipeDetailsPage",
    component: RecipeDetailsPage,
  },
  {
    path: "/add-recipe",
    name: "AddRecipePage",
    component: AddRecipePage
  },
];

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: routes
});

export default router;
