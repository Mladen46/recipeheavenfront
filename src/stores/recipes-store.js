import axios from "axios";

export const getRecipes = (callbacks) => {
    axios.get("/recipes").then(callbacks.onSuccess).catch(callbacks.onError);
}

export const getRecipeById = (id, callbacks) => {
    return axios.get(`/recipes/${id}`).then(callbacks.onSuccess).catch(callbacks.onError)
}

export const getIngredients = (callbacks) => {
    axios.get("/recipes/ingredients").then(callbacks.onSuccess).catch(callbacks.onError);
}

export const addRecipe = (recipe, callbacks) => {
    axios.post("/recipes", recipe).then(callbacks.onSuccess).catch(callbacks.onError);
}

export const updateRecipeImage = (recipeId, imageData, callbacks) => {
    axios({
        method: "post",
        url: "/recipes/image?recipeId=" + recipeId,
        data: imageData,
        headers: { "Content-Type": "multipart/form-data" },
    }).then(callbacks.onSuccess).catch(callbacks.onError)
}